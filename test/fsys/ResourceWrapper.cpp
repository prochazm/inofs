#include "gtest/gtest.h"
#include "util//ResourceWrapper.hpp"


TEST(ResourceWrapperTest, RAII) {
    auto target = 0;
    auto source = 1;

    {
        auto wrap = ResourceWrapper<int>(&source, [&](){ target = source; });
        EXPECT_EQ(target, 0);
        EXPECT_EQ(source, 1);
    }

    EXPECT_EQ(target, 1);
}

TEST(ResourceWrapperTest, get) {
    auto value = 0x71236A;
    auto wrap = ResourceWrapper<int>(&value, [](){});

    EXPECT_EQ(&value, wrap.get());
}

TEST(ResourceWrapperTest, arrowOperator) {
    struct Data { int value = 0xAB54; };

    auto value = Data();
    auto wrap = ResourceWrapper<Data>(&value, [](){});

    EXPECT_EQ(wrap->value, value.value);
}
