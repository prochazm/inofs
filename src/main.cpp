#include <iostream>
#include <filesystem>

#include "repl/Shell.hpp"

int main(
        [[maybe_unused]] int argc, 
        [[maybe_unused]] char** argv) 
{
    try {
        if (argc != 2) return 1;
        Shell(argv[1]).run();
    } catch (const char* err) {
        std::cerr << err << "\n";
    }

    return 0;
}
