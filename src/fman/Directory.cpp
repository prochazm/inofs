#include <sstream>
#include <algorithm>

#include "Directory.hpp"
#include "util/Exception.hpp"


Directory::Directory(std::istream& source) {
    deserialize(source);
}

Directory::Directory(std::uint32_t inode, std::uint32_t parentInode) {
    insert(DIRECTORY_THIS,  inode);
    insert(DIRECTORY_PARENT, parentInode);
}

std::uint32_t Directory::getInode() const {
    return content.at(DIRECTORY_THIS);
}

std::optional<std::uint32_t> Directory::getInodeOpt(const std::string& fileName) const {
    if (contains(fileName)) {
        return content.at(fileName);
    }
    return std::nullopt;
}

std::uint32_t Directory::getInode(const std::string& fileName) const {
    try {
        return content.at(fileName);
    } catch (const std::out_of_range&) {
        throw EXCEPTION("entry not found", Exception::PATH_RESOLUTION);
    }
}

const std::string& Directory::getName(const std::uint32_t inode) const {
    return std::find_if(content.cbegin(), content.cend(), [inode](const auto& pair){
        return pair.second == inode;
    })->first;
}

bool Directory::contains(const std::string& fileName) const {
    return content.contains(fileName);
}

bool Directory::isEmpty() const {
    return content.size() <= 2;
}

bool Directory::isRoot() const {
    return getInode() == getInode(DIRECTORY_PARENT);
}

void Directory::insert(const std::string& fileName, const std::uint32_t inodeId) {
    if (fileName.size() > FILE_NAME_SIZE) {
        throw EXCEPTION("file name too long.", Exception::LIMITS_REACHED);
    }
    content.emplace(fileName, inodeId);
}

void Directory::remove(const std::string& fileName) {
    content.erase(fileName);
}

void Directory::remove(const std::uint32_t inode) {
    remove(getName(inode));
}

void Directory::forEach(std::function<void(std::pair<const std::string&, const std::uint32_t>)> fn) const {
    std::for_each(content.cbegin(), content.cend(), fn);
}

void Directory::serialize(std::ostream& target) { 
    forEach([&target](std::pair<const std::string&, const std::uint32_t>pair) {
        target.write(pair.first.c_str(), FILE_NAME_SIZE);
        target.write(reinterpret_cast<const char *>(&pair.second), sizeof(std::uint32_t));
    });
    
}

void Directory::deserialize(std::istream& source) {
    content.clear();
    
    char name[FILE_NAME_SIZE];
    std::uint32_t inode;

    while (source.read(name, FILE_NAME_SIZE)) {
        source.read(reinterpret_cast<char *>(&inode), sizeof(std::uint32_t));
        content.emplace(name, inode);
    }
}

std::size_t Directory::binarySize() const {
    return ENTRY_SIZE * content.size();
}
