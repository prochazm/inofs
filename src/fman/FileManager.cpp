#include <sstream>

#include "FileManager.hpp"
#include "util/Exception.hpp"


ResourceWrapper<Directory> FileManager::getDirectory(const std::uint32_t inode) {
    loadDirectory(inode);
    
    return ResourceWrapper<Directory>(
        &directories.at(inode), 
        [this, inode](){ this->writeDirectory(inode); }
    );
}

const Directory& FileManager::viewDirectory(const std::uint32_t inode) {
    loadDirectory(inode);
    return directories.at(inode);
}

std::uint32_t FileManager::getWorkingDirectoryInode() {
    return cwd->getInode();
}

std::uint32_t FileManager::getInode(const std::string& path, bool parent) {
    return actionDispatch<std::uint32_t>(path,
        [&](const auto& dir) -> std::uint32_t 
            { return parent ? throw EXCEPTION("is a directory", Exception::PATH_RESOLUTION) : dir.getInode(); },
        [&](const auto& dir, const auto& file) -> std::uint32_t 
            { return parent ? dir.getInode() : dir.getInode(file); }
    );
}

void FileManager::loadDirectory(const std::uint32_t inode) {
    if (!directories.count(inode)) {
        if (fs.inode(inode).type != INodeFileSystem::DIRECTORY) {
            throw EXCEPTION("is not a directory", Exception::PATH_RESOLUTION);
        }
        std::stringstream ss;
        fs.read(inode, ss);
        directories.emplace(inode, ss);
    }
}

void FileManager::writeDirectory(const std::uint32_t inode) {
    Directory& dir = directories.at(inode);
    std::stringstream ss;

    fs.realloc(inode, dir.binarySize());
    dir.serialize(ss);
    fs.write(inode, ss);
}

const Directory& FileManager::selectBase(const std::string& path) {
    return path[0] == '/' ? viewDirectory(0) : *cwd;
}

const Directory& FileManager::getDeepmost(const Directory& base, std::queue<std::string>& tokenizedPath) {
          auto* dir = &base;
    const auto& next = [&dir,&tokenizedPath]() -> std::optional<std::uint32_t> { 
        return tokenizedPath.empty() ? std::nullopt : dir->getInodeOpt(tokenizedPath.front());
    };
    const auto& isDirectory = [this](const auto& inode) {
        return inode.has_value() && fs.inode(inode.value()).type == INodeFileSystem::DIRECTORY;
    };

    for (auto inode = next(); isDirectory(inode); inode = next()) {
        dir = &viewDirectory(inode.value());
        tokenizedPath.pop();
    }
    
    return *dir;
}

template<typename ReturnType>
ReturnType FileManager::actionDispatch(
    const std::string& path,
    std::function<ReturnType(const Directory&)> directoryAction,
    std::function<ReturnType(const Directory&, const std::string& name)> fileAction
){
    auto  tokPath = util::tokenize(path, '/');
    auto& deepmost = getDeepmost(selectBase(path), tokPath);

    // fileAction will get triggered even if file does not exist!
    switch(tokPath.size()) {
        case 0:  return directoryAction(deepmost);
        case 1:  return fileAction(deepmost, tokPath.front());
        default: throw  EXCEPTION("path not found", Exception::PATH_RESOLUTION);
    }
}

std::uint32_t FileManager::tryAlloc(
    const Directory& target, const std::string& name, const std::uint32_t size, const bool directory
) {
    const auto& inode = fs.alloc(directory ? INodeFileSystem::DIRECTORY : INodeFileSystem::BLOB, size);

    try {
        getDirectory(target.getInode())->insert(name, inode);
    } catch (...) { // clean up and rethrow
        fs.free(inode);
        throw;
    }

    return inode;
}

FileManager::FileManager(const std::string& fileSystemPath) : fs(fileSystemPath) { 
    loadDirectory(0);
    cwd = &directories.at(0);
}

void FileManager::write(const std::string& path, std::istream& source, const std::uint32_t size) {
    actionDispatch<void>(path,
            [ ](const auto&){ throw EXCEPTION("writing to directory", Exception::UNSUPPORTED_OPERATION); },
            [&](const auto& dir, const auto& file){
                std::uint32_t inode;
                if (dir.contains(file)) {
                    inode = dir.getInode(file);
                    fs.realloc(inode, size);
                } else {
                    inode = tryAlloc(dir, file, size, false);
                }
                fs.write(inode, source);
            }
    );
}

void FileManager::allocDirectory(const std::string& path) {
    actionDispatch<void>(path,
        [ ](const auto&) { throw EXCEPTION("directory exists", Exception::PATH_COLLISION); },
        [&](const auto& dir, const auto& file) { 
            if (dir.contains(file)) {
                throw EXCEPTION("file with same name exists", Exception::PATH_COLLISION); 
            }
            const auto& inode = tryAlloc(dir, file, 0, true);
            directories.insert_or_assign(inode, Directory(inode, dir.getInode()));
            writeDirectory(inode);
        }
    );
}

void FileManager::read(const std::string& path, std::ostream& target) {
    actionDispatch<void>(path,
        [ ](const auto&){ throw EXCEPTION("reading directory", Exception::FILE_NOT_FOUND); },
        [&](const auto& dir, const auto& file){ fs.read(dir.getInode(file), target); }
    );
}

void FileManager::remove(const std::string& path) {
    actionDispatch<void>(path,
        [this](const auto& dir) {
            if (!dir.isEmpty()) throw EXCEPTION("directory is not empty", Exception::DIRECTORY_NOT_EMPTY);
            if (dir.isRoot())   throw EXCEPTION("trying to delete root", Exception::UNSUPPORTED_OPERATION);
            if (&dir == cwd)    changeDirectory("/");
            fs.free(dir.getInode());
            getDirectory(dir.getInode(".."))->remove(dir.getInode()); // remove reference from parent
        },
        [this](const auto& dir, const auto& file) { 
            fs.free(dir.getInode(file)); 
            getDirectory(dir.getInode())->remove(file);
        }
    );
}

const Inode& FileManager::dump(const std::uint32_t inode) {
    return fs.inode(inode);
}

void FileManager::changeDirectory(const std::string& path) {
    cwd = actionDispatch<const Directory*>(path,
        [&](const auto& dir) -> const Directory* 
            { return &dir; },
        [&](const auto&, const auto&) -> const Directory*
            { throw EXCEPTION("path is a file", Exception::PATH_RESOLUTION); }
    );
}


void FileManager::init(const std::string& fileSystemPath, const std::size_t size) {
    INodeFileSystem::format(fileSystemPath, size);

    INodeFileSystem ifs(fileSystemPath);
    Directory root(0, 0); 
    
    // if fs allocs non-zero inode then throw
    if (ifs.alloc(INodeFileSystem::DIRECTORY, root.binarySize()) != 0) {
        throw EXCEPTION("allocated non-zero root inode", Exception::INITIALIZATION_FAILURE);
    }

    std::stringstream ss;
    root.serialize(ss);
    ifs.write(0, ss);
}

const std::string FileManager::pathToDirectory(const std::uint32_t inode) {
    auto childInode = inode;
    auto dir = viewDirectory(viewDirectory(inode).getInode(".."));
    std::string path("");

    // ascend the hierarchy until "." == ".." i.e. dir is root
    while (childInode != dir.getInode()) {
        path = "/" + dir.getName(childInode) + path;
        childInode = dir.getInode();
        dir = viewDirectory(dir.getInode(".."));
    }

    // return "/" if cwd is root result would be "" otherwise
    return path.length() ? path : "/";
}

void FileManager::fsck(std::ostream& logger) {
    fs.fsck(logger); // perform L1 fsck first

    const auto& bitmap = fs.inodeBitmap();
    const auto  inodeCount = fs.header().inodeCount;
    std::map<std::uint32_t, std::uint32_t> allocatedInodes;

    // find all allocated inodes
    for (std::uint32_t i = 0; i < inodeCount; ++i) {
        if (bitmap.getBit(i)) {
            allocatedInodes.emplace(i, 0);
        }
    }
    ++allocatedInodes.at(0); // root is not refered from anywhere

    // remove reachable inodes
    using argType = std::pair<const std::string&, const std::uint32_t>;
    std::function<void(argType)> markOccurrence;
    markOccurrence = [&](argType pair) {
        if (!pair.first.compare(Directory::DIRECTORY_THIS)
            || !pair.first.compare(Directory::DIRECTORY_PARENT)){
            return;
        }
        if (allocatedInodes.contains(pair.second)) {
            if (allocatedInodes.at(pair.second) > 0) {
                logger << "Info: found multi-ref inode#" << pair.second << ", named: " << pair.first << ", just saying...\n";
            } else {
                ++allocatedInodes.at(pair.second);
            }
        } else {
            logger << "Info: found link-to-free inode#" << pair.second << ", reallocating\n";
            fs.realloc(pair.second, fs.inode(pair.second).fileSize);
            allocatedInodes.emplace(pair.second, 0);
        }
        if (fs.inode(pair.second).isDirectory()) {
            viewDirectory(pair.second).forEach(markOccurrence);
        }
    }; // lambda recursion, how sick is that?

    const auto& rootDir = viewDirectory(0);
    rootDir.forEach(markOccurrence);


    // remove refered inodes
    std::erase_if(allocatedInodes, [](const auto& item){ return item.second; });
    // unrefered inodes to LOST+FOUND
    if (allocatedInodes.size() > 0) {
        if (!rootDir.contains("LOST+FOUND")){
            logger << "Info: creating LOST+FOUND directory\n";
            allocDirectory("/LOST+FOUND");
        }
        auto dirLostFound = getDirectory(rootDir.getInode("LOST+FOUND"));
        std::for_each(allocatedInodes.cbegin(), allocatedInodes.cend(), [&](const auto& pair){
            if (fs.inode(pair.first).isDirectory()){
                logger << "Info: orphaned directory at inode#" << pair.first << ", removing\n";
                fs.free(pair.first); // force remove directories
            } else {
                logger << "Info: orphaned file at inode#" << pair.first << ", linking\n";
                dirLostFound->insert(std::to_string(pair.first), pair.first); // move files to L+F
            }
        });
    }
}

void FileManager::unlinkFile(const std::string& path) {
    actionDispatch<void>(path,
        [&](const auto& dir) {
            getDirectory(dir.getInode(".."))->remove(dir.getInode());
        },
        [&](const auto& dir, const auto& file) {
            getDirectory(dir.getInode())->remove(file);
        }
    );
}

void FileManager::hardClone(const std::string& source) {
    actionDispatch<void>(source,
        [&](const auto&) { throw EXCEPTION("directories are not supported for this action", Exception::UNSUPPORTED_OPERATION); },
        [&](const auto& dir, const auto& file) {
            const auto inodeId = fs.alloc(INodeFileSystem::BLOB, 0);
            fs.hardClone(dir.getInode(file), inodeId);
            getDirectory(dir.getInode())->insert(std::to_string(inodeId), inodeId);
        }
    );
}

void FileManager::breakBitmaps() {
    fs.breakBitmaps();
}
