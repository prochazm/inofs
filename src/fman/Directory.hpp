#pragma once

#include <string>
#include <functional>
#include <vector>
#include <map>
#include <optional>


class Directory {
    private:
        std::map<std::string, std::uint32_t> content;

    public:
        static constexpr std::size_t FILE_NAME_SIZE = 12;
        static constexpr std::size_t ENTRY_SIZE = FILE_NAME_SIZE + sizeof(std::uint32_t);
        static constexpr char DIRECTORY_THIS[] = ".";
        static constexpr char DIRECTORY_PARENT[] = "..";

        Directory() = delete;
        Directory(std::istream& source);
        Directory(std::uint32_t inode, std::uint32_t parentInode);

        std::uint32_t getInode() const;
        std::uint32_t getInode(const std::string& fileName) const;
        std::optional<std::uint32_t> getInodeOpt(const std::string& fileName) const;

        const std::string& getName(const std::uint32_t inode) const;

        bool contains(const std::string& fileName) const;
        bool isEmpty() const;
        bool isRoot() const;
        void insert(const std::string& fileName, const std::uint32_t inodeId);
        void remove(const std::string& fileName);
        void remove(const std::uint32_t inode);
        void forEach(std::function<void(std::pair<const std::string&, const std::uint32_t >)> fn) const;

        void serialize(std::ostream& target);
        void deserialize(std::istream& source);
        std::size_t binarySize() const;

};
