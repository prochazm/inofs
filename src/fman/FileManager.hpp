#pragma once

#include <iostream>
#include <queue>

#include "Directory.hpp"
#include "fsys/INodeFileSystem.hpp"


class FileManager {
    private:
        INodeFileSystem fs;
        std::map<std::uint32_t, Directory> directories;
        Directory const* cwd; // cwd is managed by directories -- naked ptr is fine here

        void loadDirectory(const std::uint32_t inode);
        void writeDirectory(const std::uint32_t inode);

        const Directory& selectBase(const std::string& path);
        const Directory& getDeepmost(const Directory& base, std::queue<std::string>& tokenizedPath);

        template<typename ReturnType>
        ReturnType actionDispatch(
            const std::string& path,
            std::function<ReturnType(const Directory&)> directoryAction,
            std::function<ReturnType(const Directory&, const std::string& name)> fileAction
        );
        std::uint32_t tryAlloc(const Directory& target, const std::string& name, const std::uint32_t size, const bool directory);
    
    public:
        FileManager() = delete;
        FileManager(const std::string& fileSystemPath);

        ResourceWrapper<Directory> getDirectory(const std::uint32_t inode);
        const Directory& viewDirectory(const std::uint32_t inode);
        std::uint32_t getWorkingDirectoryInode();
        std::uint32_t getInode(const std::string& path, bool parent);
        const Inode& dump(const std::uint32_t inode); 
        const std::string pathToDirectory(const std::uint32_t inode);

        /** Writes from source stream to path exactly size
         *  bytes of arbitrary data.
         *
         *  @param path - file system path
         *  @param source - data source
         *  @param size - data size
         */
        void write(const std::string& path, std::istream& source, const std::uint32_t size);

        /** Allocates empty directory which is going to be
         *  reachable by path.
         *
         *  @param path - path to new directory
         */
        void allocDirectory(const std::string& path);

        /** Reads arbitrary data from path to target.
         *  Data are being writen until inode is fully
         *  read.
         *  
         *  @param path - path to object
         *  @param target - target stream
         */
        void read(const std::string& path, std::ostream& target);

        /** Removes arbitrary object. In case the object
         *  is a directory the deletion fails unless it
         *  is empty.
         *
         *  @param path - path to object
         */
        void remove(const std::string& path);

        /** Changes current working directory to one
         *  specified by path.
         *
         *  @param path - path to new cwd
         */
        void changeDirectory(const std::string& path);

        /** Performs consistency check and attempts to
         *  fix revealed inconsistencies.
         *
         *  @param logger - stream for logging information
         */
        void fsck(std::ostream& logger);

        /** Deletes link to file from its parent directory.
         *
         *  @param - path to file
         */
        void unlinkFile(const std::string& path);

        /** Clones inode, puts the clone beside the
         *  original and names it the id of clone inode.
         *
         *  @param source - path to clonee
         */
        void hardClone(const std::string& source);

        /// Passthrough to L1
        void breakBitmaps();

        static void init(const std::string& fileSystemPath, const std::size_t size);
};
