#include "Shell.hpp"



Shell::Shell(const std::string& filesystem) : cmd(filesystem) { }

const std::string Shell::stopLooping(const auto&) {
    looping = false;
    return "TERMINATING";
}

const std::string Shell::loadCommands(const auto& file) {
    std::ifstream ifstream(util::tokenize(file, ' ').front());
    if (!ifstream) {
        throw EXCEPTION("failed to open ifstream", Exception::FILE_NOT_FOUND);
    }
    run(ifstream);
    return "OK";
}

void Shell::run(std::istream& input) {
    const bool isCin = &input == &std::cin;
    looping = true; // insert quit command to break loop
    std::string line;

    while (looping) {
        std::cout << cmd.getFSPath() << ":" << cmd.printDirectory("") << "$ ";

        if (!std::getline(input, line)) break;
        if (!isCin) std::cout << line << "\n";
        
        const auto  spacePos = std::min(line.find_first_of(' ', 0), line.length());
        const auto& cmd = line.substr(0, spacePos);
        const auto& args = line.substr(spacePos, std::string::npos);

        try {
            const auto  result = dispatchTable.at(cmd)(args);
            std::cout << result << "\n";
        } catch (const Exception& e) {
            std::cerr << e.message << "\n";
            std::cout << errorMessageTable.at(e.cause) << "\n";
        } catch (const std::out_of_range& oor) {
            std::cout << "INVALID OPERATION, AVAILABLE OPTIONS ARE:\n";
            std::for_each(dispatchTable.cbegin(), dispatchTable.cend(), 
                [](const auto& pair){ std::cout << pair.first << "\n"; });
        }
    }
}

void Shell::run() {
    run(std::cin);
}
