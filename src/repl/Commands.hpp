#pragma once

#include <memory>

#include "fman/FileManager.hpp"


class Commands {
    private:
        static constexpr char MSG_OK[]  = "OK";

        const std::string& fileSystemPath;
        std::unique_ptr<FileManager> fileManager;

        auto parseArgs(const std::string& args, unsigned count);
    
    public:
        Commands() = delete;
        Commands(const std::string& path);

        const std::string load(const std::string& args);
        const std::string extract(const std::string& args);

        const std::string copy(const std::string& args);
        const std::string move(const std::string& args);
        const std::string remove(const std::string& args);
        const std::string print(const std::string& args);

        const std::string makeDirectory(const std::string& args);
        const std::string listDirectory(const std::string& args);
        const std::string changeDirectory(const std::string& args);
        const std::string printDirectory(const std::string&);

        const std::string dumpInode(const std::string& args);
        const std::string format(const std::string& args);

        const std::string fsck(const std::string&);
        const std::string hardClone(const std::string& args);
        const std::string unlinkFile(const std::string& args);
        const std::string breakBitmaps(const std::string&);

        const std::string& getFSPath();
};
