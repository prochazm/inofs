#pragma once

#include <string>
#include <map>
#include <functional>

#include "Commands.hpp"
#include "util/Exception.hpp"

#define CALLABLE(func) [this](const auto& a) { return func(a); }

class Shell {
    private:
        Commands cmd;

        const std::map<const std::string, std::function<const std::string(const std::string&)>> dispatchTable {
            { "cp",     CALLABLE(cmd.copy) },
            { "mv",     CALLABLE(cmd.move) },
            { "rm",     CALLABLE(cmd.remove) },
            { "mkdir",  CALLABLE(cmd.makeDirectory) },
            { "rmdir",  CALLABLE(cmd.remove) },
            { "ls",     CALLABLE(cmd.listDirectory) },
            { "cat",    CALLABLE(cmd.print) },
            { "cd",     CALLABLE(cmd.changeDirectory) },
            { "pwd",    CALLABLE(cmd.printDirectory) },
            { "info",   CALLABLE(cmd.dumpInode) },
            { "incp",   CALLABLE(cmd.load) },
            { "outcp",  CALLABLE(cmd.extract) },
            { "format", CALLABLE(cmd.format) },
            { "check",  CALLABLE(cmd.fsck) },
            { "hc",     CALLABLE(cmd.hardClone) },
            { "ul",     CALLABLE(cmd.unlinkFile) },
            { "bb",     CALLABLE(cmd.breakBitmaps) },
            { "load",   CALLABLE(loadCommands) },
            { "quit",   CALLABLE(stopLooping) },
        };
        const std::map<const Exception::Cause, const std::string> errorMessageTable {
            { Exception::PATH_RESOLUTION,        "FILE NOT FOUND"},
            { Exception::PATH_COLLISION,         "EXISTS"},
            { Exception::PATH_NOT_FOUND,         "PATH NOT FOUND"},
            { Exception::FILE_NOT_FOUND,         "FILE NOT FOUND"},
            { Exception::DIRECTORY_NOT_EMPTY,    "NOT EMPTY"},
            { Exception::UNSUPPORTED_OPERATION,  "UNSUPPORTED OPERATION"},
            { Exception::LIMITS_REACHED,         "LIMITS REACHED"},
            { Exception::INVALID_ARGUMENTS,      "PARSING ERROR"},
            { Exception::INITIALIZATION_FAILURE, "CANNOT CREATE FILE" },
        };

        bool looping = false;

        void readEvalPrint(std::istream& source);
        const std::string stopLooping(const auto&);
        const std::string loadCommands(const auto&);
    
    public:
        Shell(const std::string& filesystem);
        
        void run(std::istream& input);
        void run();

};
