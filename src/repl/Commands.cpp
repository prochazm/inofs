#include <filesystem>

#include "Commands.hpp"
#include "util/Exception.hpp"

#define REMAP_RETHROW(original_cause, new_cause) \
    (Exception& e) { \
    if (e.cause == (original_cause)) e.cause = (new_cause);\
    throw; }

Commands::Commands(const std::string& path) : fileSystemPath(path) {  
    try {
        fileManager = std::make_unique<FileManager>(path);
    } catch (const Exception& e) { // could not initialize FS
        if (e.cause != Exception::INITIALIZATION_FAILURE) {
            throw;
        }

        std::cerr << e.message << "\n";
        std::cout << "Info: trying to format with default size 65MiB\n";
        FileManager::init(path, 1 << 26);
        fileManager = std::make_unique<FileManager>(path);
    }
}

auto Commands::parseArgs(const std::string& args, unsigned count) {
    const auto& tokens = util::tokenize(args, ' ');

    if (tokens.size() != count) {
        throw EXCEPTION("argument count mismatch", Exception::INVALID_ARGUMENTS);
    }

    return tokens;
}

const std::string Commands::load(const std::string& args) {
    auto tokens = parseArgs(args, 2);

    std::ifstream source(tokens.front());
    if (!source) {
        throw EXCEPTION("ifstream to source file could not be opened", Exception::FILE_NOT_FOUND);
    }
    
    try {
        fileManager->write(tokens.back(), source, std::filesystem::file_size(tokens.front()));
    } catch REMAP_RETHROW(Exception::PATH_RESOLUTION, Exception::PATH_NOT_FOUND);

    return MSG_OK;
}

const std::string Commands::extract(const std::string& args) {
    auto tokens = parseArgs(args, 2);

    std::ofstream target(tokens.back());
    if (!target) {
        throw EXCEPTION("ofstream to target file could not be opened", Exception::PATH_NOT_FOUND);
    }

    fileManager->read(tokens.front(), target);
    
    return MSG_OK;
}


const std::string Commands::format(const std::string& args) {
    auto size = [this, args](){
        auto str = parseArgs(args, 1).front();
        std::size_t size = std::stoi(str.substr(0, str.size() - 2));
        switch (str[str.size() - 2]) {
            case 'T': size *= 1024; /* fall through */
            case 'G': size *= 1024; /* fall through */
            case 'M': size *= 1024; /* fall through */
            case 'K': size *= 1024; /* fall through */

        }
        return size;
    }();

    FileManager::init(fileSystemPath, size);
    fileManager = std::make_unique<FileManager>(fileSystemPath);

    return MSG_OK;
}


const std::string Commands::copy(const std::string& args) {
    auto tokens = parseArgs(args, 2);
    
    std::stringstream ss;
    auto inode = fileManager->dump(fileManager->getInode(tokens.front(), false));
    fileManager->read(tokens.front(), ss);
    try { 
        fileManager->write(tokens.back(), ss, inode.fileSize); 
    } catch REMAP_RETHROW(Exception::PATH_RESOLUTION, Exception::PATH_NOT_FOUND)

    return MSG_OK;
}


const std::string Commands::move(const std::string& args) {
    auto  tokens = parseArgs(args, 2);
    auto& source = tokens.front();
    auto& target = tokens.back();
    auto  dirSource = fileManager->getDirectory(fileManager->getInode(source, true));
    const auto sourceFile = util::tokenize(source, '/').back();
    const auto sourceInode = dirSource->getInode(sourceFile);

    try {
        auto dirTarget = fileManager->getDirectory(fileManager->getInode(target, true));
        const auto targetFile = util::tokenize(target, '/').back();

        if (dirTarget->contains(targetFile)) {
            fileManager->remove(target); // delete target if it exists
        }
        dirTarget->insert(targetFile, sourceInode);
    } catch REMAP_RETHROW(Exception::PATH_RESOLUTION, Exception::PATH_NOT_FOUND)

    dirSource->remove(sourceFile);

    return MSG_OK;
}

const std::string Commands::remove(const std::string& args) {
    fileManager->remove(parseArgs(args, 1).front());
    return MSG_OK;
}

const std::string Commands::print(const std::string& args) {
    std::stringstream ss;
    fileManager->read(parseArgs(args, 1).front(), ss);
    return ss.str();
}

const std::string Commands::makeDirectory(const std::string& args) {
    try {
        fileManager->allocDirectory(parseArgs(args, 1).front());
    } catch REMAP_RETHROW(Exception::PATH_RESOLUTION, Exception::PATH_NOT_FOUND)
    
    return MSG_OK;
}

const std::string Commands::listDirectory(const std::string& args) {
    auto tokens = parseArgs(args, 1);
    try {
        const auto& dir = fileManager->viewDirectory(
            fileManager->getInode(tokens.front(), false));
        std::string output("");

        dir.forEach([this, &output](const auto& p){
            output += fileManager->dump(p.second).isDirectory() ? "+" : "-";
            output += p.first + "\t» " + std::to_string(p.second) + "\n";
        });

        return output.substr(0, output.length() - 1); // trim last \n
    } catch REMAP_RETHROW(Exception::PATH_RESOLUTION, Exception::PATH_NOT_FOUND)
}

const std::string Commands::changeDirectory(const std::string& args) {
    const auto& tokens = parseArgs(args, 1);

    try {
        fileManager->changeDirectory(tokens.front());
    } catch REMAP_RETHROW(Exception::PATH_RESOLUTION, Exception::PATH_NOT_FOUND)

    return MSG_OK;
}

const std::string Commands::printDirectory(const std::string&) {
    return fileManager->pathToDirectory(fileManager->getWorkingDirectoryInode());
}

const std::string Commands::dumpInode(const std::string& args) {
    const auto& tokens = parseArgs(args, 1);
    const auto& inodeId = fileManager->getInode(tokens.front(), false);
    const auto& inode = fileManager->dump(inodeId);
    const auto& name = [&]() {
        if (inode.isDirectory()) {
            return fileManager->viewDirectory( // child inode
                fileManager->viewDirectory(inodeId).getInode("..") // parent inode id
            ).getName(inodeId); // child directory name
        }
        return fileManager->viewDirectory( // parent directory
            fileManager->getInode(tokens.front(), true) // parent inode id
        ).getName(inodeId); // child file name
    }();

    std::stringstream ss;
    ss << name << ": " << inode.fileSize << "B/" << inode.blockRefCount << "R - ino_" << inodeId;
    ss << " - DL:";
    std::for_each(inode.directLink.cbegin(), inode.directLink.cend(), 
        [&ss](const auto& link){ ss << " " << link; });
    ss << " - IL: " << inode.indirectLink;
    ss << " - DIL: " << inode.doublyIndirectLink;
    return ss.str();
}

const std::string Commands::fsck(const std::string&) {
    fileManager->fsck(std::cout);
    return MSG_OK;
}

const std::string Commands::hardClone(const std::string& args) {
    fileManager->hardClone(parseArgs(args, 1).front());
    return MSG_OK;
}

const std::string Commands::unlinkFile(const std::string& args) {
    fileManager->unlinkFile(parseArgs(args, 1).front());
    return MSG_OK;
}

const std::string Commands::breakBitmaps(const std::string&) {
    fileManager->breakBitmaps();
    return MSG_OK;
}

const std::string& Commands::getFSPath() {
    return fileSystemPath;
}
