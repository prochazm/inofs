#include "Inode.hpp"
#include "util/util.hpp"


void Inode::serialize(std::ostream& target) {
    util::writeMany(directLink, target);
    util::write(indirectLink, target);
    util::write(doublyIndirectLink, target);
    util::write(blockRefCount, target);
    util::write(fileSize, target);
    util::write(type, target);
}


void Inode::deserialize(std::istream& source) {
    util::readMany(directLink, source);
    util::read(indirectLink, source);
    util::read(doublyIndirectLink, source);
    util::read(blockRefCount, source);
    util::read(fileSize, source);
    util::read(type, source);
}
