#pragma once

#include <string>

#include "Serializer.hpp"


class INodeFileSystem {
    private:
        Serializer serializer;

        /// internal routines -- do NOT expose to file managers
        std::uint32_t balloc();
        void freeBlocks(const auto inodeId);
        void increase(const auto inodeId, const auto size);

    public:
        enum FileType {
            DIRECTORY = 0, 
            BLOB      = 1, 
        };
        
        INodeFileSystem() = delete;
        INodeFileSystem(const std::string& path) : serializer(path) {};

        const Inode&  inode(const std::size_t id);
        const Bitmap& inodeBitmap();
        const Header& header();

        /** Allocate inode with predefined size.
         *  Blocks are allocated and linked to inode
         *  to match the size.
         *
         *  @param inoType - directory or file
         *  @param size - preallocated size
         *  @return inode id
         */
        std::uint32_t alloc(const FileType inoType, const std::uint32_t size);

        /** Resize inode to match new size.
         *
         *  @param id - inode id
         *  @param size - new size
         */
        void realloc(const std::size_t id, const std::uint32_t size);

        /** Free inode and associated blocks.
         *
         *  @param id - inode id
         */
        void free(const std::size_t id);

        /** Rewrites inode with data contained in source stream.
         *
         *  @param id - inode id
         *  @param source - data source
         */
        void write(const std::size_t id, std::istream& source);

        /** Reads data associated with inode defined by it id.
         *  Data is then writen to target stream.
         *
         *  @param id - inode id
         *  @param target - data target
         */
        void read(const std::size_t id, std::ostream& target);

        /** Performs L1 fsck and tries to fix what is broken.
         *  L1 fsck tests consistency of inode-block linkage
         *  and block allocations.
         *
         *  @param logger - output stream for logging actions
         */
        void fsck(std::ostream& logger);

        /** Flips some bits in inode and block bitmaps.
         *  This is useful to simulate allocation errors
         *  which then can be fixed with fsck.
         */
        void breakBitmaps();

        /** Makes inode with targetId binary copy of inode with
         *  sourceId. This is useful for simulating L1 multiref
         *  error, i.e. two inodes share blocks.
         *
         *  @param sourceId - clonee
         *  @param sourceId - clone
         */
        void hardClone(std::uint32_t sourceId, std::uint32_t targetId);

        /** Formats file system at specified path to passed size.
         *  The size is meant as maximum -- file system will never
         *  be bigger than this value.
         *
         *  @param path - path to file system
         *  @param size - max size
         */
        static void format(const std::string& path, const std::size_t size);
};
