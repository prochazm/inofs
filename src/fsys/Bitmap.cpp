#include "Bitmap.hpp"
#include "util/Exception.hpp"


constexpr char Bitmap::chunkMask(std::size_t id) {
    return 0x01 << id % util::bitsof<std::byte>();
}

Bitmap::Bitmap(std::size_t size)
    : bytes(util::bytesForBits(size), 0) {}

Bitmap::Bitmap(std::size_t size, std::istream& source)
    : bytes(util::bytesForBits(size)) {
    deserialize(source);
}

char& Bitmap::chunkRef(const std::size_t id) {
    return bytes.at(id / util::bitsof<char>());
}

bool Bitmap::getBit(const std::size_t id) const {
    return bytes.at(id / util::bitsof<char>()) & chunkMask(id);
}

void Bitmap::setBit(const std::size_t id) {
    chunkRef(id) |= chunkMask(id);
}

void Bitmap::unsetBit(std::size_t id) {
    chunkRef(id) &= ~chunkMask(id);
}

void Bitmap::toggleBit(std::size_t id) {
    chunkRef(id) ^= chunkMask(id);
}

std::size_t Bitmap::firstFree() const {
    for (std::size_t i = 0; i < bytes.size(); ++i) {
        if (bytes[i] == -1) continue;
        const auto byte = bytes[i];
        for (auto j = 0; j < 8; ++j) {
            if (!(byte & (1 << j))) {
                return i * 8 + j;
            }
        }
    }
    throw EXCEPTION("exhausted.", Exception::LIMITS_REACHED);
}

std::size_t Bitmap::getBinarySize() const {
    return bytes.size();
}

void Bitmap::serialize(std::ostream& target) {
    target.write(bytes.data(), getBinarySize());
}

void Bitmap::deserialize(std::istream& source) {
    source.read(bytes.data(), getBinarySize());
}
