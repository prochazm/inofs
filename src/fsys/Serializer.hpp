#pragma once

#include <cstdint>
#include <fstream>
#include <map>

#include "Header.hpp"
#include "Bitmap.hpp"
#include "Inode.hpp"
#include "util/ResourceWrapper.hpp"
#include "util/Exception.hpp"


/**
 * Only understands FileSystem Header, other filesystem
 * structures are treated as binary blobs which are to
 * be writen to a speacial places inside the file.
 */
class Serializer {
    private:
        using Block = std::vector<char>;

        std::fstream storage;

        Header header;
        Bitmap inodeBitmap;
        Bitmap blockBitmap;
        std::map<const std::size_t, Inode> inodes;
        std::map<const std::size_t, Block> blocks;

        std::uint32_t locateInode(const std::uint32_t id);
        std::uint32_t locateBlock(const std::uint32_t id);
        std::uint32_t locateInodeBitmap();
        std::uint32_t locateBlockBitmap();
        constexpr std::uint32_t locateHeader();
        void loadBlock(const std::uint32_t id);

    public:
        Serializer(const std::string& storagePath) 
            : storage(storagePath, std::ios::in | std::ios::out | std::ios::binary)
            , header(storage)
            , inodeBitmap(header.inodeCount, storage)
            , blockBitmap(header.blockCount, storage)
        {  
            if (!storage) {
                throw EXCEPTION("failed to open fs", Exception::INITIALIZATION_FAILURE);
            }
        };


        ResourceWrapper<Header>        getHeader();
        ResourceWrapper<Bitmap>        getInodeBitmap();
        ResourceWrapper<Bitmap>        getBlockBitmap();
        ResourceWrapper<Inode>         getInode(const std::size_t id);
        ResourceWrapper<Block>         getBlock(const std::size_t id);
        ResourceWrapper<std::uint32_t> getBlockUInt32(const std::size_t id);

        const Header& viewHeader();
        const Bitmap& viewInodeBitmap();
        const Bitmap& viewBlockBitmap();
        const Inode&  viewInode(const std::size_t id);
        const Block&  viewBlock(const std::size_t id);


        void forSingleInodeDataBlock(
                const std::size_t inodeId,
                const std::size_t blockNo,
                std::function<void(std::uint32_t&)> fn);

        void forEachInodeDataBlock(
                const std::size_t inodeId,
                std::function<void(std::uint32_t&)> fn);

        void forEachInodeLinkBlock(
                const std::size_t inodeId,
                std::function<void(std::uint32_t&)> fn);

        void forEachInodeBlock(
                const std::size_t inodeId,
                std::function<void(std::uint32_t&)> fnData,
                std::function<void(std::uint32_t&)> fnLink);
        
        void writeHeader();
        void writeInodeBitmap();
        void writeBlockBitmap();
        void writeInode(const auto id);
        void writeBlock(const auto id);

        void setHeader(Header header);

        static void init(
            const std::string &path, Header &header, const std::size_t size);
};
