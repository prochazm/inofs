#include <iostream>

#include "Header.hpp"
#include "util/util.hpp"


void Header::serialize(std::ostream& target) {
    target.write(reinterpret_cast<char *>(&blockSize ), sizeof(blockSize));
    target.write(reinterpret_cast<char *>(&blockCount), sizeof(blockCount));
    target.write(reinterpret_cast<char *>(&inodeCount), sizeof(inodeCount));
}

void Header::deserialize(std::istream& source) {
    source.read(reinterpret_cast<char *>(&blockSize ), sizeof(blockSize));
    source.read(reinterpret_cast<char *>(&blockCount), sizeof(blockCount));
    source.read(reinterpret_cast<char *>(&inodeCount), sizeof(inodeCount));
}
