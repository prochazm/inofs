#pragma once

#include <cstdint>
#include <vector>
#include <iostream>

#include "util/util.hpp"


class Bitmap {
        // do NOT ever resize this!
        std::vector<char> bytes;

        static constexpr char chunkMask(const std::size_t id);
        char& chunkRef(const std::size_t id);


    public:
        Bitmap(std::size_t size);
        Bitmap(std::size_t size, std::istream& source);

        void setBit(const std::size_t id);
        void toggleBit(const std::size_t id);
        void unsetBit(const std::size_t id);
        bool getBit(const std::size_t id) const;
        std::size_t firstFree() const;

        std::size_t getBinarySize() const;
        void serialize(std::ostream& target);
        void deserialize(std::istream& source);
};
