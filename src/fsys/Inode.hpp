#pragma once

#include <cstdint>
#include <iostream>
#include <array>


class Inode {
    public:
        static constexpr auto DLINK_COUNT = std::uint32_t(6);
        static constexpr auto INODE_SIZE = 
            sizeof(std::uint32_t) * (4 + DLINK_COUNT) + sizeof(std::uint8_t);

        constexpr bool isDirectory() const { return !type; }

        std::array<std::uint32_t, DLINK_COUNT> directLink = { 0 };
        std::uint32_t indirectLink = 0;
        std::uint32_t doublyIndirectLink = 0;
        std::uint32_t blockRefCount = 0;
        std::uint32_t fileSize = 0;
        std::uint8_t  type = 0;

        Inode(std::istream& source) { deserialize(source); }

        void serialize(std::ostream& target);
        void deserialize(std::istream& source);
            
};
