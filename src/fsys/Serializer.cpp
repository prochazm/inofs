#include <vector>
#include <algorithm>

#include "Serializer.hpp"


constexpr std::uint32_t Serializer::locateHeader() {
    return 0;
}

std::uint32_t Serializer::locateInodeBitmap() {
    return locateHeader() + Header::BINARY_SIZE;
}

std::uint32_t Serializer::locateBlockBitmap() {
    return locateInodeBitmap() + inodeBitmap.getBinarySize();
}

std::uint32_t Serializer::locateInode(const std::uint32_t id) {
    return locateBlockBitmap() + blockBitmap.getBinarySize() + Inode::INODE_SIZE * id;
}

std::uint32_t Serializer::locateBlock(const std::uint32_t id) {
    return locateInode(header.inodeCount) + header.blockSize * id;
}

ResourceWrapper<Header> Serializer::getHeader() {
    return ResourceWrapper<Header>(&header, [this]{ writeHeader(); });
}

ResourceWrapper<Bitmap> Serializer::getInodeBitmap() {
    return ResourceWrapper<Bitmap>(&inodeBitmap, [this]{ writeInodeBitmap(); });
}

ResourceWrapper<Bitmap> Serializer::getBlockBitmap(){
    return ResourceWrapper<Bitmap>(&blockBitmap, [this]{ writeBlockBitmap(); });
}

ResourceWrapper<Inode> Serializer::getInode(const std::size_t id) {
    if (!inodes.count(id)) {
        storage.seekg(locateInode(id));
        inodes.emplace(id, storage);
    }

    return ResourceWrapper<Inode>(&inodes.at(id), [this, id](){ writeInode(id); });
}

void Serializer::loadBlock(const std::uint32_t id) {
    Block block(header.blockSize);

    storage.seekg(locateBlock(id));
    storage.read(block.data(), header.blockSize);
    blocks.insert({id, std::move(block)});
}

ResourceWrapper<Serializer::Block> Serializer::getBlock(const std::size_t id) {
    if (!blocks.count(id)) {
        loadBlock(id);
    }

    return ResourceWrapper<Block>(&blocks.at(id), [this, id](){ writeBlock(id); });
}

const Header& Serializer::viewHeader() {
    return header;
}

const Bitmap& Serializer::viewInodeBitmap() {
    return inodeBitmap;
}

const Bitmap& Serializer::viewBlockBitmap() {
    return blockBitmap;
}

const Inode&  Serializer::viewInode(const std::size_t id) {
    if (!inodes.count(id)) {
        storage.seekg(locateInode(id));
        inodes.emplace(id, storage);
    }

    return inodes.at(id);
}

const Serializer::Block&  Serializer::viewBlock(const std::size_t id) {
    if (!blocks.count(id)) {
        loadBlock(id);
    }

    return blocks.at(id);
}

ResourceWrapper<std::uint32_t> Serializer::getBlockUInt32(const std::size_t id) {
    if (!blocks.count(id)) {
        loadBlock(id);
    }
    
    return ResourceWrapper<std::uint32_t>(
            reinterpret_cast<std::uint32_t *>(blocks.at(id).data()),
            [this, id](){ writeBlock(id); });
}

void Serializer::forEachInodeDataBlock(
    const std::size_t inodeId,
    std::function<void(std::uint32_t&)> fn
){
    forEachInodeBlock(inodeId, fn, [](auto const){});
}

void Serializer::forEachInodeLinkBlock(
    const std::size_t inodeId,
    std::function<void(std::uint32_t&)> fn
) {
    forEachInodeBlock(inodeId, [](auto const){}, fn);
}

void Serializer::forEachInodeBlock(
    const std::size_t inodeId,
    std::function<void(std::uint32_t&)> fnData,
    std::function<void(std::uint32_t&)> fnLink
){
    auto inode = getInode(inodeId);
    const auto dataRefs = util::ceilDiv(inode->fileSize, header.blockSize);
    
    std::for_each_n(inode->directLink.begin(), std::min(dataRefs, Inode::DLINK_COUNT), fnData);

    if (dataRefs > Inode::DLINK_COUNT) {
        fnLink(inode->indirectLink);

        auto singleLinkBlock = getBlock(inode->indirectLink);
        const auto links = reinterpret_cast<std::uint32_t *>(singleLinkBlock.get()->data());
        const auto linksSize = std::min(dataRefs - Inode::DLINK_COUNT, header.blockLinkCap());

        std::for_each_n(links, linksSize, fnData);
    }

    if (dataRefs > Inode::DLINK_COUNT + header.blockLinkCap()) {
        fnLink(inode->doublyIndirectLink);

        auto blocksToRead = dataRefs - Inode::DLINK_COUNT - header.blockLinkCap();
        auto doubleLinkBlock = getBlock(inode->doublyIndirectLink);
        const auto indirectLinks = reinterpret_cast<std::uint32_t *>(doubleLinkBlock.get()->data());
        const auto indirectLinksSize = util::ceilDiv(blocksToRead, header.blockLinkCap());

        std::for_each_n(indirectLinks, indirectLinksSize, [&](auto &i) {
            fnLink(i);

            auto directLinkBlock = getBlock(i);
            const auto directLinks = reinterpret_cast<std::uint32_t *>(directLinkBlock.get()->data());

            std::for_each_n(directLinks, std::min(blocksToRead, header.blockLinkCap()), fnData );
            blocksToRead -= header.blockLinkCap();
        });
    }
}

void Serializer::writeHeader() {
    storage.seekp(locateHeader());
    header.serialize(storage);
}

void Serializer::writeInodeBitmap() {
    storage.seekp(locateInodeBitmap());
    inodeBitmap.serialize(storage);
}

void Serializer::writeBlockBitmap() {
    storage.seekp(locateBlockBitmap());
    blockBitmap.serialize(storage);
}

void Serializer::writeInode(const auto id) {
    storage.seekp(locateInode(id));
    inodes.at(id).serialize(storage);
}

void Serializer::writeBlock(const auto id) {
    storage.seekp(locateBlock(id));
    storage.write(blocks.at(id).data(), header.blockSize);
}

void Serializer::setHeader(Header header) {
    this->header = header;
    writeHeader(); // maybe just flush immediately?
}

void Serializer::init(
    const std::string &path, Header &header, const std::size_t size) 
{
    std::ofstream file(path, std::ios::out | std::ios::trunc | std::ios::binary);
    if (!file) { // oh crap!
        throw "Err: [Serializer#init] failed to open file";
    }
    header.serialize(file);         // serialize header
    file.seekp(size - 1);           // jump at last index
    file.write("", sizeof(char));   // write zero byte to resize
}
