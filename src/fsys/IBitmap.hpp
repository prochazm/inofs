#pragma once

#include <cstdint>
#include <memory>
#include <concepts>


struct IBitmap {
    IBitmap() = delete;

    // filesystem level capabilities
    virtual void setBit(const std::uint32_t id) = 0;
    virtual void toggleBit(const std::uint32_t id) = 0;
    virtual void unsetBit(const std::uint32_t id) = 0;
    virtual bool getBit(const std::uint32_t id) = 0;

    // serialization level capabilities
    virtual std::size_t getBinarySize() = 0;
    virtual void serialize() = 0;
};
