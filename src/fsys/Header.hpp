#pragma once

#include <cstdint>
#include <memory>
#include <array>

#include "Inode.hpp"
#include "util/util.hpp"


class Header {
    public:
        static constexpr auto BINARY_SIZE = sizeof(std::uint32_t) * 3;

        std::uint32_t blockSize = 0;
        std::uint32_t blockCount = 0;
        std::uint32_t inodeCount = 0;
        
        constexpr std::uint32_t blockLinkCap() { 
            return blockSize / sizeof(std::uint32_t); 
        }

        constexpr std::uint32_t dataBlocksPerFile() {
            return Inode::DLINK_COUNT + blockLinkCap() + util::pow2(blockLinkCap());
        }

        void serialize(std::ostream& target);
        void deserialize(std::istream& source);

        Header() {};
        Header(std::istream& source) {
            deserialize(source);
        }
};
