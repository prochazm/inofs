#include <cstdint>
#include <algorithm>

#include "INodeFileSystem.hpp"
#include "util/util.hpp"


std::uint32_t INodeFileSystem::balloc() {
          auto blkmap = serializer.getBlockBitmap();
    const auto availId = blkmap->firstFree();

    blkmap->setBit(availId);

    return availId;
}

void INodeFileSystem::freeBlocks(const auto inodeId) {
    auto blkmap = serializer.getBlockBitmap();
    auto inode  = serializer.getInode(inodeId);
    const auto &freeBlock = [&blkmap](auto const i){ blkmap->unsetBit(i); };

    serializer.forEachInodeBlock(inodeId, freeBlock, freeBlock);
    inode->fileSize = 0;
    inode->blockRefCount = 0;
}

void INodeFileSystem::increase(const auto inodeId, const auto targetSize) {
    auto inode = serializer.getInode(inodeId);
    inode->fileSize = targetSize;
    std::uint32_t blockOrd = 0;
    const auto& setAlloc = [&](auto& blockId){ 
        if (blockOrd++ >= inode->blockRefCount) {
            blockId = balloc(); 
            inode->blockRefCount++;
        }
    };

    serializer.forEachInodeBlock(inodeId, setAlloc, setAlloc);
}

std::uint32_t INodeFileSystem::alloc(
    const INodeFileSystem::FileType inoType, const std::uint32_t size)
{
          auto inomap = serializer.getInodeBitmap();
    const auto availId = inomap->firstFree();
          auto inode = serializer.getInode(availId);
    
    inomap->setBit(availId);
    inode->type = inoType;
    increase(availId, size);

    return availId;
}

void INodeFileSystem::realloc(const std::size_t id, const std::uint32_t size) {
    auto inode = serializer.getInode(id);
    auto header = serializer.viewHeader();

    if (size > header.blockSize * header.dataBlocksPerFile()) {
        throw EXCEPTION("file is full", Exception::LIMITS_REACHED);
    }

    // set bit just in case
    serializer.getInodeBitmap()->setBit(id);

    if (util::ceilDiv(size, header.blockSize)
        == util::ceilDiv(inode->fileSize, header.blockSize))
    { // same ammount of blocks is needed -- adjust size
        inode->fileSize = size;
        return;
    } 

    if (size < inode->fileSize) {
        freeBlocks(id); // new size is smaller, scratch all blocks
    }

    increase(id, size); // allocate blocks to match new size
}

void INodeFileSystem::free(const std::size_t id) {
    freeBlocks(id);
    serializer.getInodeBitmap()->unsetBit(id);
}

void INodeFileSystem::write(const std::size_t id, std::istream& source) {
    auto header = serializer.getHeader();
    const auto writeChunk = [&](const auto &i){
        source.read(serializer.getBlock(i)->data(), header->blockSize);
    };

    serializer.forEachInodeDataBlock(id, writeChunk);
}

void INodeFileSystem::read(const std::size_t id,  std::ostream& target) {
    auto header = serializer.getHeader();
    auto sizeToRead = serializer.getInode(id)->fileSize;
    const auto readChunk = [&](const auto &i) {
        target.write(
            serializer.getBlock(i)->data(), 
            std::min(header->blockSize, sizeToRead) 
        ); // last block might not be aligned and printing garbage is undesirable
        sizeToRead -= header->blockSize;
    };

    serializer.forEachInodeDataBlock(id, readChunk);
}

void INodeFileSystem::fsck(std::ostream& logger) {
    std::map<std::uint32_t, std::pair<std::uint32_t, bool>> allocTable; //FIXME that bool might be redundant
    auto blockBitmap = serializer.getBlockBitmap();
    auto inodeBitmap = serializer.getInodeBitmap();

    if (!inodeBitmap->getBit(0)) {
        logger << "Info: root inode is not allocated, reallocating\n";
        inodeBitmap->setBit(0);
    }

    const auto& setAlloc = [&](const auto& id) {
        if (blockBitmap->getBit(id)) {
            allocTable.emplace(id, std::pair(std::uint32_t(0), true));
        }
    };
    const auto& setRef = [&](auto& id) {
        if (allocTable.contains(id)) {
            if (allocTable.at(id).first > 0) { // multiple references
                const auto blkCopyId = balloc(); // allocate new block
                const auto blkOld  = serializer.viewBlock(id);
                serializer.getBlock(blkCopyId)->assign(blkOld.cbegin(), blkOld.cend()); // copy old contents
                logger << "Info: multi-ref detected, block#" << id << " split, copy at block#" << blkCopyId << "\n";
                id = blkCopyId; // link to new block
            } else { // first occurence, just increment
                ++allocTable.at(id).first;
            }
        } else { // block not allocated
            allocTable.emplace(id, std::pair(std::uint32_t(1), false));
            blockBitmap->setBit(id); // allocate
            logger << "Info: link-to-free detected, reallocating block#" << id << "\n";
        }
    };

    // find allocations
    for (std::uint32_t id = 0; id < header().blockCount; ++id) {
        setAlloc(id);
    }

    // find references
    for (std::uint32_t id = 0; id < header().inodeCount; ++id) {
        serializer.forEachInodeBlock(id, setRef, setRef);
    }

    // free lost blocks
    std::for_each(allocTable.cbegin(), allocTable.cend(), [&](const auto& b){
        if (!b.second.first) {
            logger << "Info: zero-ref-alloc detected, releasing block#" << b.first << "\n";
            blockBitmap->unsetBit(b.first);
        }
    });
}

void INodeFileSystem::breakBitmaps() {
    const auto& header = serializer.viewHeader();
    auto inobm = serializer.getInodeBitmap();
    auto blkbm = serializer.getBlockBitmap();

    for (std::uint32_t i = 0; i < header.inodeCount; i += 2) {
        inobm->toggleBit(i);
    }

    for (std::uint32_t i = 0; i < header.blockCount; i += 16) {
        blkbm->toggleBit(i);
    }
}

void INodeFileSystem::hardClone(std::uint32_t sourceId, std::uint32_t targetId){
    *serializer.getInode(targetId).get() = *serializer.getInode(sourceId).get();
}

void INodeFileSystem::format(const std::string& path, const std::size_t maxSize) {
    Header header = Header();

    header.blockSize = [maxSize]() {
             if (maxSize < 4525   ) throw EXCEPTION("File system is too small", Exception::INITIALIZATION_FAILURE);
        else if (maxSize < 1ul<<20) return    8; // debugging mode
        else if (maxSize < 1ul<<30) return  512;
        else if (maxSize < 1ul<<40) return 2048;
        else                        return 8192;
    }();
    // as magical as it gets...
    // I guess I should put the initial equations into the docs, shouldn't I? :P
    static constexpr std::size_t BLK_PER_INO = 64;
    const std::size_t fsMultiplier = (maxSize - Header::BINARY_SIZE) 
        / (65 + 8*Inode::INODE_SIZE + 8*BLK_PER_INO*header.blockSize);
    header.inodeCount = fsMultiplier * 8;
    header.blockCount = fsMultiplier * 8*BLK_PER_INO;

    // Result should be 64 blocks per inode with the bitmaps perfectly aligned
    // and the whole filesystem not overlapping specified size.
    const auto actualSize = [&header]() -> std::size_t {
        return Header::BINARY_SIZE 
            + header.inodeCount / 8 + header.inodeCount * Inode::INODE_SIZE 
            + header.blockCount / 8 + header.blockCount * header.blockSize;
    };

    const auto sizeDiff = [&actualSize, maxSize]() -> std::size_t {
        return maxSize - actualSize();
    };

    // fill up remaining space with blocks and then inodes
    header.blockCount += 8 * (sizeDiff() / (1 + 8*header.blockSize));
    header.inodeCount += 8 * (sizeDiff() / (1 + 8*Inode::INODE_SIZE));
    //                       ^                                      ^_
    //                       !_ these parenthesis are fucking crutial_|

    // now the size of fs should not be more than 8*INODE_SIZE + 1 bytes smaller
    // than the requested maxSize 
    Serializer::init(path, header, actualSize());
}

const Inode& INodeFileSystem::inode(const std::size_t id) {
    return serializer.viewInode(id);
}

const Bitmap& INodeFileSystem::inodeBitmap() {
    return serializer.viewInodeBitmap();
}

const Header& INodeFileSystem::header() {
    return serializer.viewHeader();
}
