#pragma once

#include <functional>


template<class T>
class ResourceWrapper {
    private:
        const std::function<void()> deleteAction;
        T *resource;

    public:
        ResourceWrapper<T>() = delete;
        ResourceWrapper<T>(T *resource, const std::function<void()> deleteAction) 
            : deleteAction(deleteAction), resource(resource) {};

        T* get() {
            return resource;
        }

        T* operator->() {
            return resource;
        }

        ~ResourceWrapper<T>() {
            deleteAction();
        }
};
