#include "util.hpp"


std::queue<std::string> util::tokenize(const std::string& str, const char delim) {
    std::queue<std::string> tokens;
    std::string lastToken;
    std::istringstream stream(str);

    while (std::getline(stream, lastToken, delim)) {
        if (lastToken.size()) {
            tokens.push(lastToken);
        }
    }

    return tokens;
}
