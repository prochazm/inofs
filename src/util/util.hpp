#pragma once

#include <cstdint>
#include <iostream>
#include <climits>
#include <concepts>
#include <queue>
#include <string>
#include <iterator>
#include <sstream>
#include <array>

namespace util {
    template<typename T, typename N> 
        requires std::integral<T> && std::integral<N>
    constexpr T ceilDiv(const T a, const N b) {
        return (a + b - 1) / b;
    }

    template<typename T>
    constexpr std::size_t bitsof() {
        return sizeof(T) << 3;
    }

    template<typename T> requires std::integral<T>
    constexpr T bytesForBits(T bitCount) {
        return ceilDiv(bitCount, CHAR_BIT);
    }
    
    template<typename T>
    constexpr void write(const T &data, std::ostream &target) {
        target.write(reinterpret_cast<const char *>(&data), sizeof(data));
    }

    template<typename T>
    constexpr void writeMany(const T &container, std::ostream& target) {
        target.write(
            reinterpret_cast<const char *>(container.data()), 
            sizeof(container[0]) * container.size()
        );
    }

    template<typename T>
    constexpr void read(T &data, std::istream &source) {
        source.read(reinterpret_cast<char *>(&data), sizeof(data));
    }
    
    template<typename T>
    constexpr void readMany(T &container, std::istream &source) {
        source.read(
            reinterpret_cast<char *>(container.data()), 
            sizeof(container[0]) * container.size()
        );
    }

    template<typename T>
    constexpr T pow2(const T base) {
        return base * base;
    }

    std::queue<std::string> tokenize(const std::string& str, const char delim);
}
