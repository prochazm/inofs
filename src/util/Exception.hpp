#pragma once

#include <string>
#include <experimental/source_location>

#define EXCEPTION(msg, cause) Exception(msg, std::experimental::source_location::current(), cause)

struct Exception {
    enum Cause {
        PATH_RESOLUTION        = 0,
        PATH_COLLISION         = 1,
        PATH_NOT_FOUND         = 2,
        FILE_NOT_FOUND         = 3,
        DIRECTORY_NOT_EMPTY    = 4,
        UNSUPPORTED_OPERATION  = 5,
        LIMITS_REACHED         = 6,
        INVALID_ARGUMENTS      = 7,
        INITIALIZATION_FAILURE = 8
    };

    const std::string message;
    Cause cause;

    
    Exception(const std::string& message, const std::experimental::source_location& location, Cause cause)
        : message(std::string("Err: [") + location.file_name() + ":" + std::to_string(location.line()) + "] " + message)
        , cause(cause) { };
};
